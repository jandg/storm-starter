package be.kahosl;

public class E5SensorValue {

	private String day, time, sensorId;
	private double kwh;
	
	public E5SensorValue(String sensorId, String day, String time, double kwh) {
		this.sensorId = sensorId;
		this.day = day;
		this.time = time;
		this.kwh = kwh;
	}
	
	public String getSensorId() {
		return this.sensorId;
	}
	
	public String getDay() {
		return this.day;
	}
	
	public String getTime() {
		return this.time;
	}
	
	public double getKwh() {
		return this.kwh;
	}
}
