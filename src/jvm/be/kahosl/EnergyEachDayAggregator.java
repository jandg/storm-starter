package be.kahosl;

import storm.trident.operation.CombinerAggregator;
import storm.trident.tuple.TridentTuple;

public class EnergyEachDayAggregator implements CombinerAggregator<Double> {
	
    @Override
    public Double init(TridentTuple tuple) {
    	return tuple.getDoubleByField("kwh");
    }

    @Override
    public Double combine(Double val1, Double val2) {
    	return val1+val2;
    }

    @Override
    public Double zero() {
        return 0D;
    }
    
}

