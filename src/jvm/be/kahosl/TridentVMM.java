package be.kahosl;



import storm.starter.trident.TridentWordCount.Split;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.builtin.Count;
import storm.trident.operation.builtin.FilterNull;
import storm.trident.operation.builtin.MapGet;
import storm.trident.operation.builtin.Sum;
import storm.trident.testing.MemoryMapState;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.LocalDRPC;
import backtype.storm.generated.StormTopology;
import backtype.storm.tuple.Fields;
import be.kahosl.spout.E5BatchSpout;

public class TridentVMM {

	public static StormTopology buildTopology(LocalDRPC drpc) {
        
		E5BatchSpout spout = new E5BatchSpout();
		
		TridentTopology topology = new TridentTopology();
		TridentState dayEnergy = topology.newStream("spout", spout)
		.groupBy(new Fields("id"))
		.persistentAggregate(new MemoryMapState.Factory(), new Fields("kwh"), new Average(), new Fields("average"))
		;
		
		 // 2 states - 1 for time and for each time the values 
		 topology.newDRPCStream("timestamp", drpc)
         .stateQuery(dayEnergy, new Fields("args"), new MapGet(), new Fields("average"))
         .each(new Fields("average"), new FilterNull())
         ;

		return topology.build();
    }
	
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		Config conf = new Config();

		LocalDRPC drpc = new LocalDRPC();
		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology("VMM", conf, buildTopology(drpc));
		for(int i=0; i<100; i++) {
	        System.out.println("2011 sensor counter: " + drpc.execute("timestamp", "10:25:00"));
	        Thread.sleep(1000);
	    }
	}

}
