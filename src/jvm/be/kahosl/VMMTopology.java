package be.kahosl;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import storm.starter.ExclamationTopology.ExclamationBolt;
import storm.starter.bolt.RollingCountBolt;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.contrib.hbase.bolts.HBaseBolt;
import backtype.storm.contrib.hbase.utils.TupleTableConfig;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.testing.TestWordSpout;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import be.kahosl.spout.E5Spout;

public class VMMTopology {

	private static final String RESULTSFILE = "results.txt";
	public static int numberOfSensors;
	
	public static class EnergyEachDayBolt extends BaseRichBolt {
		private String day;
		private double kWhStartDay;
		private OutputCollector collector;
		private static final Logger LOG = Logger.getLogger(EnergyEachDayBolt.class);
		
		
        @Override
        public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
            this.collector = collector;
        }
        
        @Override
        public void execute(Tuple tuple) {
        	// get values from tuple
        	String day = String.valueOf(tuple.getValueByField("day"));
        	double kwh = Double.parseDouble(String.valueOf(tuple.getValueByField("kwh")));
        	String time = String.valueOf(tuple.getValueByField("time"));
        	
        	LOG.info("Received sensor tuple \tday: " + day + "\ttime: " + tuple.getValueByField("time") + "\tkWh: " + kwh);
        	
        	if (this.day == null) {
        		this.day = day;
        		this.kWhStartDay = kwh;
        	}
        	else if (time.equals("23:55:00")) {
        		VMMTopology.writeToFile(this.day, (kwh-kWhStartDay), "Total energy used");
        		LOG.info("Energy used on " + day + ": " + (kwh-kWhStartDay));
        		collector.emit(new Values(tuple.getValueByField("id"), this.day, (kwh-kWhStartDay)));
        		this.day = null;
        	}      	
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("id", "day", "kWhThisDay"));
			
		}
		
		
		
	}

	public static class AverageKwhEachHour extends BaseBasicBolt {

		@Override
		public void execute(Tuple input, BasicOutputCollector collector) {
			
			double kwhEachHour = Double.parseDouble(String.valueOf(input.getValueByField("kWhThisDay"))) / 24;
			System.out.println("Average on " + String.valueOf(input.getValueByField("day")) + ": " + kwhEachHour);
			VMMTopology.writeToFile(String.valueOf(input.getValueByField("day")), kwhEachHour, "Average kwh each hour by sensor " + String.valueOf(input.getValueByField("id")));
			collector.emit(new Values(input.getValueByField("id"), input.getValueByField("day"), kwhEachHour));
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("id", "day", "AverageKwhEachHour"));
			
		}
	}
	
	public static class AverageKwhEachHourMultipleSensors extends BaseBasicBolt {

		double total = 0;
		Map<String, String> sensors = new HashMap<String, String>();
		
		@Override
		public void execute(Tuple input, BasicOutputCollector collector) {
			
			total += input.getDoubleByField("AverageKwhEachHour"); 
			sensors.put(input.getStringByField("id"), String.valueOf(input.getDoubleByField("AverageKwhEachHour")));
			
			if (sensors.size() == VMMTopology.numberOfSensors) {
				
				String sensorsOutput = "[";
				for (String sensor : sensors.keySet()) sensorsOutput += sensor + " ";
				sensorsOutput += "]";
				double average = total/sensors.size();
				System.out.println("Average for sensors " + sensorsOutput + " on " + input.getStringByField("day") + ": " + average);
				VMMTopology.writeToFile(String.valueOf(input.getValueByField("day")), average, "Average kwh each hour for all sensors");
				collector.emit(new Values(input.getStringByField("day"), average));
				
				//clear fields
				total = 0;
				sensors = new HashMap<String, String>();
			}
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("day", "all"));
			
		}
	}

	public static class ControlSensorValue extends BaseRichBolt{

		private OutputCollector collector;
		private double previousValue;
		
		@Override
		public void prepare(Map stormConf, TopologyContext context,
				OutputCollector collector) {
			
			this.collector = collector;
		}

		@Override
		public void execute(Tuple input) {

			double kwh = Double.parseDouble(String.valueOf(input.getValueByField("kwh")));
			if (kwh < this.previousValue) {
				System.out.println("Measured value is smaller than previous: " + kwh + " < " + this.previousValue);
			}
			collector.emit(new Values(input.getValueByField("id"), input.getValueByField("day"), input.getValueByField("time"), input.getValueByField("kwh")));
			this.previousValue = kwh;

		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("id", "day", "time", "kwh"));
		}
		
	}
	/**
	 * @param args
	 * @throws InvalidTopologyException 
	 * @throws AlreadyAliveException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException, InterruptedException {
		
		clearResultsFile();
		numberOfSensors = 2;
		
		TopologyBuilder builder = new TopologyBuilder();
		
	    // Build TupleTableConifg
	    TupleTableConfig config = new TupleTableConfig("vmmE5", "day");
	    config.setBatch(false);
	    config.addColumn("averageSensor", "all");
	    
		builder.setSpout("sensor2010", new E5Spout(2010), 1); 
		builder.setSpout("sensor2011", new E5Spout(2011), 1);
		//builder.setSpout("sensor2009", new E5Spout(2009), 1);
		builder.setBolt("controle", new ControlSensorValue(), 3)
				//.shuffleGrouping("sensor2009")
				.fieldsGrouping("sensor2010", new Fields("id"))
                .fieldsGrouping("sensor2011", new Fields("id"));
        builder.setBolt("day", new EnergyEachDayBolt(), 3)
        		.fieldsGrouping("controle", new Fields("id"));
        builder.setBolt("averageHour", new AverageKwhEachHour())
				.shuffleGrouping("day");
        builder.setBolt("averageHourSensors", new AverageKwhEachHourMultipleSensors())
        		.localOrShuffleGrouping("averageHour");
        builder.setBolt("hbase", new HBaseBolt(config), 1).shuffleGrouping("averageHourSensors");
        
        
        
        Config conf = new Config();
        
        if(args!=null && args.length > 0) {
            conf.setNumWorkers(3);
            
            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
        } else {
        
        	LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("EnergyEachDay", conf, builder.createTopology());
        }
		

	}
	
	private static void clearResultsFile() {
		// clear results content
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(RESULTSFILE);
			writer.print("");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}
		
		
	}

	private static void writeToFile(String day, double kwh, String message) {
		try {
			
	    	PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(RESULTSFILE, true)));
			
			pw.println(day + "\t" + kwh + "\t" + message);
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
