package be.kahosl.spout;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public class E8Spout extends BaseRichSpout {
	
	public class E8SensorValue {
		
		private String day, time;
		private double kwh;
		
		public E8SensorValue(String day, String time, double kwh) {
			this.day = day;
			this.time = time;
			this.kwh = kwh;
		}
		
		public String getDay() {
			return this.day;
		}
		
		public String getTime() {
			return this.time;
		}
		
		public double getKwh() {
			return this.kwh;
		}
	}
	
	private SpoutOutputCollector collector;
	private ArrayList<E8SensorValue> sensorValues = new ArrayList<E8Spout.E8SensorValue>();
	private int counter;
	
	@Override 
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		this.collector = collector;
		// read sensor values from E8 file
		try {
			Scanner s = new Scanner(new File("E8.txt")).useDelimiter("\n");
			int lines = 0;
			while (s.hasNext()) {
				String line = s.next().replaceAll("[\\t\\r]", " ");
				if (lines > 1) {
					String [] fields = line.split(" ");
				    this.sensorValues.add(new E8SensorValue(fields[0], fields[1], Double.parseDouble(fields[2])));
				}
				lines++;
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		this.counter = 0;
	}
	
	@Override
    public void nextTuple() {
		//every 0.1s
        Utils.sleep(100);
        E8SensorValue value = this.sensorValues.get(counter);
        Map<String, String> data = new HashMap<String, String>();
        data.put("day", value.getDay());
        data.put("time", value.getTime());
        data.put("kwh", String.valueOf(value.getKwh()));
        
        this.collector.emit(new Values(data));
        counter++;
    }

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("sensorValue"));		
	}  
	

}
