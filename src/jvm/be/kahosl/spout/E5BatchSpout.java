package be.kahosl.spout;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import be.kahosl.E5SensorValue;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

@SuppressWarnings({ "serial", "rawtypes" })
public class E5BatchSpout implements IBatchSpout {

	private int counter;
	private ArrayList<E5SensorValue> sensorValues2010 = new ArrayList<E5SensorValue>();
	private ArrayList<E5SensorValue> sensorValues2011 = new ArrayList<E5SensorValue>();
	

	@Override
	public void open(Map conf, TopologyContext context) {
		sensorValues2010 = getValuesFromFile(2010);
		sensorValues2011 = getValuesFromFile(2011);
		this.counter = 0;
	}

	private ArrayList<E5SensorValue> getValuesFromFile(int year) {
		// read sensor values from E5 file
		ArrayList<E5SensorValue> fileLines = new ArrayList<E5SensorValue>();
		try {
				Scanner s = new Scanner(new File("E5-"+year+".txt")).useDelimiter("\n");
				int lines = 0;
				while (s.hasNext()) {
					String line = s.next().replaceAll("[\\t\\r]", " ");
					if (lines > 1) {
						String [] fields = line.split(" ");
					    fileLines.add(new E5SensorValue(String.valueOf(year), fields[0], fields[1], Double.parseDouble(fields[2])));
					}
					lines++;
				}
				s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return fileLines;
		
	}
	
	@Override
	public void emitBatch(long batchId, TridentCollector collector) {
		// emit batchSize with sensor values
		for (Values v : getNextSensorValues()) collector.emit(v);
		
	}

	@Override
	public void ack(long batchId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map getComponentConfiguration() {
		return new Config();
	}

	@Override
	public Fields getOutputFields() {
		return new Fields("id", "day", "time", "kwh");
	}
	
	/**
	 * batch values for 2 sensors (ids: 2010 and 2011)
	 * 
	 * change dates from 2010 to 2011, to simulate two sensors at the same time
	 * kwh fields represent energy used in the last time offset
	 * @return
	 */
	private Values[] getNextSensorValues() {
		Values[] values = new Values[2];
		if (counter <= sensorValues2010.size()-1) {
			E5SensorValue value = this.sensorValues2010.get(counter);
			double offset = (counter == 0) ? 0 : (value.getKwh() - this.sensorValues2010.get(counter - 1).getKwh());
			value.getDay().replaceAll("2010", "2011");
	        values[0] = new Values(value.getSensorId(), value.getDay(), value.getTime(), offset);
		}
		if (counter <= sensorValues2011.size()-1) {
			E5SensorValue value = this.sensorValues2011.get(counter);  
			double offset = (counter == 0) ? 0 : (value.getKwh() - this.sensorValues2011.get(counter - 1).getKwh());
	        values[1] =  new Values(value.getSensorId(), value.getDay(), value.getTime(), offset);
		}
		counter++;
		return values;
	}
	
	

}
